# Do not modify this file!  It was generated by ‘nixos-generate-config’
# and may be overwritten by future invocations.  Please make changes
# to /etc/nixos/configuration.nix instead.
{
  lib,
  modulesPath,
  ...
}: {
  imports = [
    (modulesPath + "/installer/scan/not-detected.nix")
  ];

  boot.initrd.availableKernelModules = ["xhci_pci" "thunderbolt" "nvme" "usb_storage" "sd_mod"];
  boot.initrd.kernelModules = [];
  boot.kernelModules = ["kvm-intel"];
  boot.extraModulePackages = [];

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/6d7acb6e-7878-47da-b153-aeb635bff529";
    fsType = "ext4";
  };

  boot.initrd.luks.devices."root-crypt".device = "/dev/disk/by-uuid/05a629be-fbf6-4a1c-8354-c48a7ea5521e";

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/B646-1FA4";
    fsType = "vfat";
  };

  swapDevices = [];

  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
}
